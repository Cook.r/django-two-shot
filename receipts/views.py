from django.shortcuts import render, get_list_or_404, get_object_or_404, redirect
from receipts.models import Receipt, Account, ExpenseCategory
from django.contrib.auth.decorators import login_required
from receipts.forms import CreateReceipt, CreateCategory, CreateAccount
# Create your views here.


@login_required
def receipt_view(request):
    receipt = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt": receipt,
    }
    return render(request, "receipts/receipts.html", context)


@login_required
def expense_view(request):
    receipt = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "receipt": receipt,
    }
    return render(request, "receipts/categories.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CreateCategory(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            form.save()
            return redirect("category_list")
    else:
        form = CreateCategory()
    context = {
        "form": form
            }
    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = CreateAccount(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            form.save()
            return redirect("account_list")
    else:
        form = CreateAccount()
    context = {
        "form": form
            }
    return render(request, "receipts/create_account.html", context)


@login_required
def account_view(request):
    receipt = Account.objects.filter(owner=request.user)
    context = {
        "receipt": receipt,
    }
    return render(request, "receipts/accounts.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = CreateReceipt(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            form.save()
            return redirect("home")
    else:
        form = CreateReceipt()
    context = {
        "form": form
            }
    return render(request, "receipts/create_receipt.html", context)
